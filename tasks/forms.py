from tasks.models import Task
from django import forms


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "assignee",
            "project",
            "start_date",
            "due_date",
        )
